libThom
===================

This **C++ library** is used for ease of use of pre defined functions made by Thomas Dwi Dinata. It is made to help developers and students creating projects or solving problems easier by using the pre-defined functions on this **header file**. Other sources from the Internet or friends of mine might affect the work of this pre-defined Library.


----------

Release Notes
-------------

#### v1.0 Beta
- Initial Release
- Functions added :
	- Terminal Colour modifier
	- OS Dependent Functions **system()**
	- Input sanitiser for number inputs
	- Stack and Queue Operations (**struct** compliant)
	- Keyboard Keypress Listener

----------

Upcoming Releases
-------------

#### v1.0 GM
- Class compliant support for Stack and Queue Operations
- Class as Linked List Manager 

----------


Library Testing 
-------------
libThom is tested on several machines in order to achieve multiple Operating System (OS) support and make it easier for development on different OS.

> The main development is currently using macOS 10.12.1(Darwin Kernel x86_64-apple-darwin16.1.0) using Xcode Version 8.1 (8B62) and Apple LLVM Compiler (clang-800.0.42.1)

- v1.0 Beta :
	- has been tested on :
		- macOS 10.12.1 (Darwin Kernel x86_64-apple-darwin16.1.0) using Compiler Apple LLVM (clang-800.0.42.1)
	- is being tested on :
		- Ubuntu 14.04 (Linux Kernel 4.4) using Compiler GCC (Version TBA)
		- Windows 10 (Version 1511 Build 10xxx.x) using Compiler TBA (Version TBA)

- v1.0 :
	- has been tested on :
		- N/A
	- is being tested on :
		- macOS 10.12.1 (Darwin Kernel x86_64-apple-darwin16.1.0) using Compiler Apple LLVM (clang-800.0.42.1)

> - If the library passes and tested on macOS and Linux, it should probably works on any POSIX Compatible OS (Not Windows, even using MinGW or Cygwin does not give any effect because Windows' command line is different with UNIX and UNIX-Like OSes)  
> - Not all functions are compatible to all OS. Such as Terminal Colour Modifier won't work on Windows

----------

Function List
-------------------

- System Functions
	- `system_pause();` = An equivalent of System Pause in Windows Command Prompt
	- `system_clear();` = Clears the Terminal Screen
- Input Sanitiser
	- `scanNumber(int *choice)` = Sanitise the `std::cin` stream when wrong data type is being recieved by it

----------

Function Description
-------------------

- Terminal Colour Modifier
	- Description : Terminal Colour Modifier defines the colour code that will be decoded by Terminal and system to produce colourful text and bold text.
	- Features :
		- Formats the output text that will be displayed on the Terminal or TTY
		- Makes fancy texts and bold texts in order to get an attention from the users to the specific text
	- Requirement :
		- UNIX or UNIX-Like OS (ex. macOS and Linux)
	- Samples : 
	  `cout << "Hello World in Normal Way" << endl << BOLDBLUE << "Hello World in Bold Blue" << endl << RESET;`
	  or
	  `printf("Hello World in Normal Way\n"BOLDBLUE"HelloWorldinBoldBlue\n"RESET);`

- System Function
	- Description : Provides the same functions to each OS. It will declares specific OS different 'system()' calls
	- Features : 
		- Provides UNIX and UNIX-Like OS to have 'pause' command from Windows
		- Call a function that suits with the running OS

- Input Sanitiser
	- Description : To sanitise the `std::cin` function in order to prevent unlimited loop when assigning a value given by user to a specific variable
	- Features :
		- Cleans `std::cin` stream when assigning a non number to any number data types
	- Samples :
		- scanNumber(*choice)
		  `int a;
		  scanNumber(&a);`
	  

----------
