//
//  libThom.hpp
//  v1.0 Beta
//  Release Notes may refer to "libThom.md"
//
//  Created by Thomas Dwi Dinata on 10/27/16.
//  Copyright © 2016 Thomas Dwi Dinata. All rights reserved.
//
//  This library is used for ease of use of pre-defined functions made by Thomas Dwi Dinata.
//  The library is intended to be used on C++ Programming Language.
//  Other sources from the Internet or friends of mine might affect the work of this pre-defined Library.
//
//  The full list of the functions and it's description are available on "libThom.md"
//

#ifndef libThom_h
#define libThom_h

#pragma once

#include <iostream>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;

// Terminal Colour Modifier START =-=-=-=-

#define RESETCOLOUR   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

// Terminal Colour Modifier STOP =-=-=-=-



// System Function START =-=-=-=-
#ifdef _WIN32
    //define something for Windows (32-bit and 64-bit, this part is common)
    #include <conio.h>
    void system_pause()
    {
        system("pause");
    }
    void system_clear_screen()
    {
        system("cls");
    }
    #ifdef _WIN64
        //define something for Windows (64-bit only)
    #endif
#elif __APPLE__
    #include "TargetConditionals.h"
    #if TARGET_IPHONE_SIMULATOR
        // iOS Simulator
    #elif TARGET_OS_IPHONE
        // iOS device
    #elif TARGET_OS_MAC
        void system_pause()
        {
            system("read -n 1 -s -p \"Press any key to continue...\"" );
        }
        void system_clear_screen()
        {
            system("clear");
        }
    #else
        #   error "Unknown Apple platform"
    #endif
#elif __linux__
    void system_pause()
    {
        cout << "Press enter to continue...";
        std::cin.get();
    }
    void system_clear_screen()
    {
        system("clear");
    }
#elif __unix__ // all unices not caught above
    // Unix
#elif defined(_POSIX_VERSION)
    // POSIX
#else
    #   error "Unknown compiler or OS"
#endif

// System Function STOP =-=-=-=-



// Input Sanitiser START =-=-=-=-

int scanNumber(int *choice) // Input sanitiser to make sure that user inputs only numbers, and clears the input stream to avoid infinite loop, and returns the integer value
{
    std::cin >> *choice;
    while(std::cin.fail())
    {
        std::cin.clear(); //This corrects the stream.
        std::cin.ignore(); //This skips the left over stream data.
        printf("Invalid response, please input numbers only : ");
        std::cin >> *choice;
    }
    return *choice;
}

// Input Sanitiser STOP =-=-=-=-



// Stack Queue (Struct) Operations START =-=-=-=->

// Stack Operations BEGIN
template<typename T>
void pushStack(T newNode, T head, T tail)
{
    newNode->next = NULL;
    newNode->prev = NULL;
    if(head == NULL && tail == NULL)
        head = tail = newNode;
    else
    {
        newNode->next = head;
        head = newNode;
    }
}

template<typename T>
void pushStackDLL(T newNode, T head, T tail) // For double linked list (DLL)
{
    newNode->next = NULL;
    newNode->prev = NULL;
    if(head == NULL && tail == NULL)
        head = tail = newNode;
    else
    {
        newNode->next = head;
        head->prev = newNode;
        head = newNode;
    }
}
// Stack Operations END

// Queue Operations BEGIN
template<typename T>
void pushQueue(T newNode, T head, T tail)
{
    newNode->next = NULL;
    newNode->prev = NULL;
    if(head == NULL && tail == NULL)
        head = tail = newNode;
    else
    {
        tail->next = newNode;
        tail = newNode;
    }
}

template<typename T>
void pushQueueDLL(T newNode, T head, T tail) // For double linked list (DLL)
{
    newNode->next = NULL;
    newNode->prev = NULL;
    if(head == NULL && tail == NULL)
        head = tail = newNode;
    else
    {
        newNode->prev = tail;
        tail->next = newNode;
        tail = newNode;
    }
    
}
// Queue Operations END

// Pop Operations START
template<typename T>
T popStruct(T head, T tail)
{
    T *toBeDeleted = head;
    head = head->next;
    delete toBeDeleted;
}
// Pop Operations END

// Create a Struct BEGIN
template<typename T, typename S>
T nodeCreator(S data, T sample)
{
    T newNode = new T();
    newNode->data = data;
    newNode->next = NULL;
}

template<typename T, typename S>
T nodeCreatorDLL(S data, T sample) // For double linked list (DLL)
{
    T newNode = new T();
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
}
// Create a Struct END

// Stack Queue (Struct) Operations START =-=-=-=->



// Keypress Handler START =-=-=-=-

bool kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;
    
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
    
    ch = getchar();
    
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);
    
    if(ch != EOF)
    {
        ungetc(ch, stdin);
        return true;
    }
    
    return false;
}

// KEY PRESS HANDLER FINISH - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#endif /* libThom_h */
